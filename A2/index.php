<?php require_once "./code.php";?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S01: PHP Basics and Selection Control structure</title>
	</head>

	<body>
		<h1>Letter-Based Grading</h1>
		<p><?php echo  getLetterGrade(99); ?></p>
		<p><?php echo  getLetterGrade(44); ?></p>
		<p><?php echo  getLetterGrade(57); ?></p>
		<p><?php echo  getLetterGrade(79); ?></p>
		<p><?php echo  getLetterGrade(89); ?></p>
		<p><?php echo  getLetterGrade(91); ?></p>
		<p><?php echo  getLetterGrade(95); ?></p>

	</body>
</html>
