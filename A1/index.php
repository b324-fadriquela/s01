<?php require_once "./code.php";?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>S01: PHP Basics and Selection Control structure</title>
	</head>

	<body>
		<div class="card">
			<h1>Full Address</h1>
				<p><?php echo  getFullAddress('Philippines', 'Metro Manila', 'Quezon City', '3F Caswynn Bldg., Timog Avenue'); ?></p>
				<p><?php echo  getFullAddress('Philippines', 'Metro Manila', 'Makati City', '3F Enzo Bldg., Buendia Avenue'); ?></p>
		</div>
	</body>
</html>
